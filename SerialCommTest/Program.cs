﻿using NmeaParser.Nmea;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Threading;

namespace SerialCommTest {
    class Program {

        private Queue<string> messages = new Queue<string>(101);

        static void Main(string[] args) {
            var port = new SerialPort("COM8", 4300);
            var device = new NmeaParser.SerialPortDevice(port);
            device.MessageReceived += Device_MessageReceived;
            device.OpenAsync(); 

            while(device.IsOpen) {
                Thread.Sleep(2000);
                //Console.WriteLine("NMEA Parser running.");
            }
        }

        /*
        private void Device_MessageReceived(object sender, NmeaParser.NmeaMessageReceivedEventArgs args) {
            messages.Enqueue(args.Message.MessageType + ": " + args.Message.ToString());
            if (messages.Count > 100) messages.Dequeue();

            if (args.Message is NmeaParser.Nmea.Gps.Gprmc) {
                var x = args.Message as NmeaParser.Nmea.Gps.Gprmc;
                Console.WriteLine(x.Latitude + ", " + x.Longitude);
            }
        }
        */
        private static void Device_MessageReceived(object sender, NmeaParser.NmeaMessageReceivedEventArgs args) {
            if (args.Message is NmeaParser.Nmea.Gps.Gprmc) {
                var x = args.Message as NmeaParser.Nmea.Gps.Gprmc;
                Console.WriteLine(x.Latitude + ", " + x.Longitude);
            }
        }
    }

    [NmeaMessageType("GPRMC")]
    public class Gprmc : NmeaMessage {
        protected override void OnLoadMessage(string[] message) {
            base.OnLoadMessage(message);
        }
    }
}
